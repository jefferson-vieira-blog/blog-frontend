import Vue from 'vue';
import App from '@/App.vue';

import router from '@/config/router';
import store from '@/config/store';
import bootstrap from '@/config/bootstrap';
import toasted from '@/config/toasted';
import sweetalert from '@/config/sweetalert';

import '@fortawesome/fontawesome-free/css/all.css';

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  bootstrap,
  toasted,
  sweetalert,
  render: h => h(App)
}).$mount('#app');
