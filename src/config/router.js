import Vue from 'vue';
import Router from 'vue-router';

import Layout from '../Layout.vue';

import Auth from '@/views/auth/AuthPage';
import Home from '@/views/home/HomePage';
import Admin from '@/views/admin/AdminPage';

import ArticleList from '@/views/article/ArticleList';
import Article from '@/views/article/Article';

import { validateToken, signout } from '@/utils/auth';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/auth',
      name: 'auth',
      component: Auth,
      beforeEnter: async (to, from, next) => {
        (await validateToken()) ? next('/home') : next();
      }
    },
    {
      path: '/',
      component: Layout,
      beforeEnter: async (to, from, next) => {
        if (await validateToken()) {
          // let token = JSON.parse(localStorage.getItem(AUTH_KEY))
          // getUser(token.access_token)
          next();
        } else {
          signout();
          next({
            path: '/auth',
            query: { redirect: to.fullPath }
          });
        }
      },
      redirect: '/home',
      children: [
        {
          path: '/home',
          name: 'home',
          component: Home
        },
        {
          path: '/admin',
          name: 'admin',
          component: Admin
        },
        {
          path: '/categories/:id/articles',
          name: 'articleList',
          component: ArticleList
        },
        {
          path: '/articles/:id',
          name: 'article',
          component: Article
        }
      ]
    }

    // {
    //   path: "/about",
    //   name: "about",
    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () =>
    //     import(/* webpackChunkName: "about" */ "./views/About.vue")
    // }
  ]
});
