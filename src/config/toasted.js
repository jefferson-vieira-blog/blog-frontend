import Vue from 'vue';
import Toasted from 'vue-toasted';

Vue.use(Toasted, {
  iconPack: 'fontawesome',
  duration: 4000
});

Vue.toasted.register('success', payload => payload, {
  type: 'success',
  icon: 'check'
});

Vue.toasted.register('warning', payload => payload, {
  icon: 'exclamation',
  duration: 1500
});

Vue.toasted.register('error', payload => payload, {
  type: 'error',
  icon: 'times'
});
