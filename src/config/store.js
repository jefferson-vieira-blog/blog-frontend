import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    loading: false,
    showMenu: false,
    user: null
  },
  getters: {
    loading: store => store.loading,
    showMenu: store => store.showMenu,
    user: store => store.user
  },
  mutations: {
    TOGGLE_LOADING(state, isLoading = !state.loading) {
      state.loading = isLoading;
    },
    TOGGLE_MENU(state, showMenu) {
      if (!state.user) {
        state.showMenu = false;
        return;
      }

      state.showMenu = showMenu === undefined ? !state.showMenu : showMenu;
    },
    SET_USER(state, user) {
      state.user = user;

      state.showMenu = !!user;
    }
  },
  actions: {
    toggleLoading(context, isLoading) {
      context.commit('TOGGLE_LOADING', isLoading);
    },
    toggleMenu(context, showMenu) {
      context.commit('TOGGLE_MENU', showMenu);
    },
    setUser(context, user) {
      context.commit('SET_USER', user);
    }
  }
});
