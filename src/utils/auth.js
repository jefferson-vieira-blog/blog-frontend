import http from './http';

import { AUTH_KEY } from '@/config';

function getToken() {
  return JSON.parse(localStorage.getItem(AUTH_KEY));
}

function saveToken(token) {
  localStorage.setItem(AUTH_KEY, JSON.stringify(token));
}

function deleteToken() {
  localStorage.removeItem(AUTH_KEY);
}

export async function sign(mode, user) {
  const { data } = await http.post(`/${mode}`, user);
  saveToken(data.token);
  return data;
}

export async function validateToken() {
  const token = getToken();
  if (!token) return false;

  const { data: user } = await http.post('/validate-token', { token });
  if (!user) deleteToken();
  return user;
}

export function initAuthInterceptor(instance) {
  instance.interceptors.request.use(
    config => {
      let token = getToken();
      if (token) config.headers.Authorization = `Bearer ${token}`;
      return config;
    },
    err => {
      return Promise.reject(err);
    }
  );
}

export async function signout() {
  deleteToken();
}
