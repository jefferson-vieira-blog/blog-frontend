import Vue from 'vue';

const msg = 'Ops... Um erro inesperado ocorreu!';

export function errorToast(error) {
  Vue.toasted.global.error(handleError(error));
}

export function errorAlert(error) {
  return Vue.swal({
    type: 'error',
    title: 'Ops!',
    confirmButtonColor: '#dc3545',
    text: handleError(error)
  });
}

function handleError(error) {
  if (typeof error === 'string') return error;
  const {
    response: { data, status }
  } = error;
  if (typeof data === 'string') return data;
  return getMessageErrorByStatus(status);
}

function getMessageErrorByStatus(status) {
  let message = '';
  switch (status) {
    case 400:
      message = 'Está enviando tudo certinho?';
      break;
    case 401:
    case 403:
      message = 'Você não tem permissões suficientes!';
      break;
    case 404:
      message = 'Não encontramos o conteúdo que você estava procurando';
      break;
    case 500:
      message = 'Houve um erro que nos impediu de completar sua solicitação';
      break;
    case 503:
      message = 'Não foi possível se comunicar com o servidor';
      break;
    default:
      message = msg;
      break;
  }
  return message;
}
