import Vue from 'vue';

export function confirmAlert(message) {
  return Vue.swal({
    type: 'question',
    title: 'Mas antes...',
    text: message,
    confirmButtonColor: '#61dafb',
    showCancelButton: true,
    confirmButtonText: 'Sim',
    cancelButtonText: 'Não'
  });
}
