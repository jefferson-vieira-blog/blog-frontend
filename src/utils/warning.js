import Vue from 'vue';

export function warningToast(message) {
  Vue.toasted.global.warning(message);
}
