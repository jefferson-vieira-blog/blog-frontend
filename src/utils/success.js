import Vue from 'vue';

const msg = 'Operação realizada com sucesso!';

export function successToast(message) {
  Vue.toasted.global.success(message);
}

export function successAlert(message) {
  return Vue.swal({
    type: 'success',
    title: 'Pronto!',
    text: message || msg,
    confirmButtonColor: '#28a745'
  });
}
