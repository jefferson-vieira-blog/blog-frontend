import http from '@/utils/http';

const path = '/articles';

export async function getArticles(page) {
  const url = `${path}${page ? `?page=${page}` : ''}`;
  const { data } = await http.get(url);
  return data;
}

export async function getArticleById(id) {
  const { data } = await http.get(`${path}/${id}`);
  return data;
}

export function createArticle(article) {
  return http.post(path, article);
}

export function updateArticle(id, article) {
  return http.put(`${path}/${id}`, article);
}

export function removeArticle(id) {
  return http.delete(`${path}/${id}`);
}
