import http from '@/utils/http';

export async function getUsers() {
  const { data } = await http.get('/users');
  return data;
}

export function createUser(user) {
  return http.post('/users', user);
}

export function updateUser(id, user) {
  return http.put(`/users/${id}`, user);
}

export function removeUser(id) {
  return http.delete(`/users/${id}`);
}
