import http from '@/utils/http';

export async function getStats() {
  const { data } = await http.get('/stats');
  return data;
}
