import http from '@/utils/http';

const path = '/categories';

export async function getCategories() {
  const { data } = await http.get(path);
  return data;
}

export async function getCategoryById(id) {
  const { data } = await http.get(`${path}/${id}`);
  return data;
}

export async function getArticlesByCategoryId(id, page) {
  const { data } = await http.get(`${path}/${id}/articles?page=${page}`);
  return data;
}

export async function getMenuItens() {
  const { data } = await http.get(`${path}/tree`);
  return data;
}

export function createCategory(category) {
  return http.post(path, category);
}

export function updateCategory(id, category) {
  return http.put(`${path}/${id}`, category);
}

export function removeCategory(id) {
  return http.delete(`${path}/${id}`);
}
